from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    if name is None:
      name = "John Doe"
    return 'Hello, ' + name

app.run(host='0.0.0.0', port=8080)
